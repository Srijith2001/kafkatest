package main

import (
	"encoding/json"
	"io/ioutil"

	"github.com/invopop/jsonschema"
	"main.go/dtos"
)

func KafkaSchemas() map[string]interface{} {
	var schemas = make(map[string]interface{})

	schemas["my-topic"] = jsonschema.Reflect(&dtos.SampleKafkaProducer{})

	file, _ := json.MarshalIndent(schemas, "", " ")

	_ = ioutil.WriteFile("test.json", file, 0644)
	return schemas
}
