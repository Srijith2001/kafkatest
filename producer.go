package main

import (
	// Import the dtos package
	"encoding/json"

	"main.go/dtos"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func main() {

	KafkaSchemas()
	// Create a new kafka producer instance
	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost:9092",
		"group.id":          "myGroup",
	})
	if err != nil {
		panic(err)
	}
	defer p.Close()

	var student dtos.SampleKafkaProducer
	student.Name = "John"
	student.Age = 30

	// Convert student to json
	jsonStudent, err := json.Marshal(student)
	if err != nil {
		panic(err)
	}
	// Push student to my-topic
	topic := "my-topic"
	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          []byte(jsonStudent),
	}, nil)
}
