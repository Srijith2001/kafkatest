package dtos

type SampleKafkaProducer struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}
