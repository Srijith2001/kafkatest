module main.go

go 1.17

require github.com/confluentinc/confluent-kafka-go v1.9.2

require (
	github.com/iancoleman/orderedmap v0.0.0-20190318233801-ac98e3ecb4b0 // indirect
	github.com/invopop/jsonschema v0.6.0 // indirect
)
